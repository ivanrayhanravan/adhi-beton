-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2021 at 09:19 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adhi_beton`
--

-- --------------------------------------------------------

--
-- Table structure for table `em_archive_test_files`
--

CREATE TABLE `em_archive_test_files` (
  `id_archive_test` int(11) NOT NULL,
  `jenis_test` varchar(255) NOT NULL,
  `nama_test` varchar(255) NOT NULL,
  `id_sample_source` int(10) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `received_date` date NOT NULL,
  `sertif_img` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_archive_test_files`
--

INSERT INTO `em_archive_test_files` (`id_archive_test`, `jenis_test`, `nama_test`, `id_sample_source`, `id_pelaksana`, `received_date`, `sertif_img`) VALUES
(4, 'Internal Test', 'Test pertama', 5, 5, '2021-07-07', 'img/content/Archive/alfin.jpg'),
(5, 'Independent Test', 'Test pertama', 6, 4, '2021-07-09', 'img/content/Archive/alfin1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `em_cache_slump`
--

CREATE TABLE `em_cache_slump` (
  `id_cache` int(11) NOT NULL,
  `id_job_mix_formula` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `em_customer`
--

CREATE TABLE `em_customer` (
  `id_customer` int(10) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_customer`
--

INSERT INTO `em_customer` (`id_customer`, `customer_name`, `status`) VALUES
(0, 'Belum Terisi', 0),
(10, 'Adhi Karya Beton', 0),
(20, 'Waskita', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_daily_compressive`
--

CREATE TABLE `em_daily_compressive` (
  `id_daily_compressive` int(11) NOT NULL,
  `id_job_mix_formula` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_product` int(10) NOT NULL,
  `id_mutu` int(10) NOT NULL,
  `date_of_pouring` date DEFAULT NULL,
  `date_test` date DEFAULT NULL,
  `id_type` int(11) NOT NULL,
  `no_produksi` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `density` varchar(100) NOT NULL,
  `load` varchar(100) NOT NULL,
  `strength_mpa` varchar(100) NOT NULL,
  `correction_to_cube` varchar(100) NOT NULL,
  `id_status` int(11) NOT NULL,
  `doc_img` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_daily_compressive`
--

INSERT INTO `em_daily_compressive` (`id_daily_compressive`, `id_job_mix_formula`, `id_customer`, `id_project`, `id_product`, `id_mutu`, `date_of_pouring`, `date_test`, `id_type`, `no_produksi`, `age`, `weight`, `density`, `load`, `strength_mpa`, `correction_to_cube`, `id_status`, `doc_img`) VALUES
(29, 57, 20, 9, 7, 4, '0000-00-00', '0000-00-00', 4, '1', '2', '3', '4', '5', '6', 'Clear', 1, 'img/content/DailyCompressive/07489c6897b049657f9e915ff9313703.jpg'),
(30, 58, 0, 0, 6, 4, NULL, NULL, 0, '', '', '', '', '', '', '', 3, ''),
(31, 59, 0, 0, 6, 4, NULL, NULL, 0, '', '', '', '', '', '', '', 3, ''),
(32, 60, 0, 0, 0, 4, NULL, NULL, 0, '', '', '', '', '', '', '', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `em_daily_incoming_additive`
--

CREATE TABLE `em_daily_incoming_additive` (
  `id_daily_incoming_additive` int(11) NOT NULL,
  `surat_jalan` varchar(50) NOT NULL,
  `id_supplier` int(10) NOT NULL,
  `merek_additive` varchar(255) NOT NULL,
  `kriteria_millsheet_sg` int(11) NOT NULL,
  `kriteria_millsheet_ph` int(11) NOT NULL,
  `kriteria_millsheet_warna` int(11) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `doc_img` longtext NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_daily_incoming_additive`
--

INSERT INTO `em_daily_incoming_additive` (`id_daily_incoming_additive`, `surat_jalan`, `id_supplier`, `merek_additive`, `kriteria_millsheet_sg`, `kriteria_millsheet_ph`, `kriteria_millsheet_warna`, `id_pelaksana`, `doc_img`, `keterangan`, `date_time`, `id_status`) VALUES
(6, 'N1234AAB', 4, 'Merek 2', 12, 12, 0, 5, 'img/content/DailyIncomingAdditive/5035ca9bde12bf81a7eb410f46210b6f.jpg', 'Mobil bocor', '2021-07-11 16:42:49', 3);

-- --------------------------------------------------------

--
-- Table structure for table `em_daily_incoming_halus`
--

CREATE TABLE `em_daily_incoming_halus` (
  `id_daily_incoming_halus` int(11) NOT NULL,
  `id_material` int(10) NOT NULL,
  `surat_jalan` varchar(50) NOT NULL,
  `id_vendor` int(10) NOT NULL,
  `id_quarry` int(10) NOT NULL,
  `kriteria_visual_butiran` varchar(11) NOT NULL,
  `kriteria_visual_kekerasan` varchar(11) NOT NULL,
  `kriteria_visual_warna` varchar(11) NOT NULL,
  `slit_content` double NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `doc_img` longtext NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_daily_incoming_halus`
--

INSERT INTO `em_daily_incoming_halus` (`id_daily_incoming_halus`, `id_material`, `surat_jalan`, `id_vendor`, `id_quarry`, `kriteria_visual_butiran`, `kriteria_visual_kekerasan`, `kriteria_visual_warna`, `slit_content`, `id_pelaksana`, `doc_img`, `keterangan`, `date_time`, `id_status`) VALUES
(6, 9, 'N1234AAB', 1, 3, 'Yes', 'Yes', 'Yes', 100, 5, 'img/content/DailyIncomingHalus/75ce080fff370f1d66d017ba3ec38873.jpg', 'Mobil bocor alus', '2021-07-11 16:40:08', 3);

-- --------------------------------------------------------

--
-- Table structure for table `em_daily_incoming_kasar`
--

CREATE TABLE `em_daily_incoming_kasar` (
  `id_daily_incoming_kasar` int(11) NOT NULL,
  `id_material` int(10) NOT NULL,
  `surat_jalan` varchar(50) NOT NULL,
  `id_vendor` int(10) NOT NULL,
  `id_quarry` int(10) NOT NULL,
  `kriteria_visual_bersih` varchar(11) NOT NULL,
  `kriteria_visual_sedang` varchar(11) NOT NULL,
  `kriteria_visual_kotor` varchar(11) NOT NULL,
  `ukuran` varchar(11) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `doc_img` longtext NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_daily_incoming_kasar`
--

INSERT INTO `em_daily_incoming_kasar` (`id_daily_incoming_kasar`, `id_material`, `surat_jalan`, `id_vendor`, `id_quarry`, `kriteria_visual_bersih`, `kriteria_visual_sedang`, `kriteria_visual_kotor`, `ukuran`, `id_pelaksana`, `doc_img`, `keterangan`, `date_time`, `id_status`) VALUES
(7, 9, 'N1234AAB', 4, 3, 'Yes', 'Yes', 'Yes', 'Yes', 5, 'img/content/DailyIncomingKasar/c3379e821321c78f0662339832e82b03.jpg', 'Mobil mogok', '2021-07-11 16:37:49', 3);

-- --------------------------------------------------------

--
-- Table structure for table `em_independent_test`
--

CREATE TABLE `em_independent_test` (
  `id_independent_test` int(11) NOT NULL,
  `id_material` int(10) NOT NULL,
  `id_sample_source` int(10) NOT NULL,
  `id_sample_description` int(11) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `received_date` date NOT NULL,
  `doc_img` longtext NOT NULL,
  `sertif_img` longtext NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_independent_test`
--

INSERT INTO `em_independent_test` (`id_independent_test`, `id_material`, `id_sample_source`, `id_sample_description`, `id_pelaksana`, `received_date`, `doc_img`, `sertif_img`, `id_status`) VALUES
(10, 3, 8, 6, 5, '2021-07-07', 'img/content/IndependentTest/line_2442341651995902.jpg', 'img/content/IndependentTest/alfin1.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `em_internal_tes_halus`
--

CREATE TABLE `em_internal_tes_halus` (
  `id_internal_tes_halus` int(11) NOT NULL,
  `id_sample_source` int(10) NOT NULL,
  `id_sample_description` int(10) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `id_to_be_used` int(10) NOT NULL,
  `received_date` date NOT NULL,
  `satu_input_satu` double NOT NULL,
  `satu_input_dua` double NOT NULL,
  `satu_input_tiga` double NOT NULL,
  `satu_input_empat` double NOT NULL,
  `satu_input_lima` double NOT NULL,
  `satu_input_enam` double NOT NULL,
  `satu_input_tujuh` double NOT NULL,
  `satu_input_fm` double NOT NULL,
  `doc_input_satu` longtext NOT NULL,
  `dua_input_satu` double NOT NULL,
  `dua_input_dua` double NOT NULL,
  `doc_input_dua` longtext NOT NULL,
  `tiga_input_satu` varchar(30) NOT NULL,
  `doc_input_tiga` longtext NOT NULL,
  `empat_input_satu` double NOT NULL,
  `doc_input_empat` longtext NOT NULL,
  `lima_input_satu` double NOT NULL,
  `doc_input_lima` longtext NOT NULL,
  `enam_input_satu` double NOT NULL,
  `doc_input_enam` longtext NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `em_internal_tes_kasar`
--

CREATE TABLE `em_internal_tes_kasar` (
  `id_internal_tes_kasar` int(11) NOT NULL,
  `id_sample_source` int(10) NOT NULL,
  `id_sample_description` int(10) NOT NULL,
  `id_pelaksana` int(10) NOT NULL,
  `id_to_be_used` int(10) NOT NULL,
  `received_date` date NOT NULL,
  `satu_input_satu` double NOT NULL,
  `satu_input_dua` double NOT NULL,
  `satu_input_tiga` double NOT NULL,
  `satu_input_empat` double NOT NULL,
  `satu_input_lima` double NOT NULL,
  `satu_input_enam` double NOT NULL,
  `satu_input_tujuh` double NOT NULL,
  `satu_input_fm` double NOT NULL,
  `doc_input_satu` longtext NOT NULL,
  `dua_input_satu` double NOT NULL,
  `dua_input_dua` double NOT NULL,
  `doc_input_dua` longtext NOT NULL,
  `tiga_input_satu` varchar(30) NOT NULL,
  `doc_input_tiga` longtext NOT NULL,
  `empat_input_satu` double NOT NULL,
  `doc_input_empat` longtext NOT NULL,
  `lima_input_satu` double NOT NULL,
  `doc_input_lima` longtext NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_internal_tes_kasar`
--

INSERT INTO `em_internal_tes_kasar` (`id_internal_tes_kasar`, `id_sample_source`, `id_sample_description`, `id_pelaksana`, `id_to_be_used`, `received_date`, `satu_input_satu`, `satu_input_dua`, `satu_input_tiga`, `satu_input_empat`, `satu_input_lima`, `satu_input_enam`, `satu_input_tujuh`, `satu_input_fm`, `doc_input_satu`, `dua_input_satu`, `dua_input_dua`, `doc_input_dua`, `tiga_input_satu`, `doc_input_tiga`, `empat_input_satu`, `doc_input_empat`, `lima_input_satu`, `doc_input_lima`, `id_status`) VALUES
(3, 5, 4, 4, 4, '2021-07-07', 11, 9, 11, 2, 9, 9, 11, 9, 'img/content/InternalKasar/fdff14bf9ffe1d43b3969da69e6b650c.jpg', 9, 11, 'img/content/InternalKasar/0074792dffed828bc0245448c4d0bd35.jpg', '9,50', 'img/content/InternalKasar/24c9a7f92301164c007f4c24619b970a.jpg', 9, 'img/content/InternalKasar/bb9b06f1acadc0dee7e03d86a5ea2644.jpg', 2, 'img/content/InternalKasar/0bcee6d14aceae8d13b4300f8ab4d912.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `em_job_mix_formula`
--

CREATE TABLE `em_job_mix_formula` (
  `id_job_mix_formula` int(11) NOT NULL,
  `id_product` int(10) NOT NULL,
  `name_jmf` varchar(100) NOT NULL,
  `id_mutu` int(10) NOT NULL,
  `id_sumber_semen` int(11) NOT NULL,
  `jumlah_semen` int(11) NOT NULL,
  `id_sumber_halus` int(11) NOT NULL,
  `jumlah_halus` int(11) NOT NULL,
  `id_sumber_kasar` int(11) NOT NULL,
  `jumlah_kasar` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_sumber_additive` int(11) NOT NULL,
  `jumlah_additive` int(11) NOT NULL,
  `jumlah_air` int(11) NOT NULL,
  `doc_img` longtext NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_job_mix_formula`
--

INSERT INTO `em_job_mix_formula` (`id_job_mix_formula`, `id_product`, `name_jmf`, `id_mutu`, `id_sumber_semen`, `jumlah_semen`, `id_sumber_halus`, `jumlah_halus`, `id_sumber_kasar`, `jumlah_kasar`, `date_time`, `id_sumber_additive`, `jumlah_additive`, `jumlah_air`, `doc_img`, `id_status`) VALUES
(57, 7, 'Job Mix Formula 1', 4, 4, 100, 6, 199, 5, 299, '2021-07-14 01:48:37', 5, 299, 399, 'img/content/JobMixFormula/alfin45.jpg', 3),
(58, 6, 'Job Mix Formula 2', 4, 5, 100, 6, 199, 5, 299, '2021-07-14 01:48:37', 5, 299, 399, 'img/content/JobMixFormula/alfin46.jpg', 3),
(59, 6, 'Job Mix Formula 3', 4, 4, 100, 6, 199, 5, 299, '2021-07-14 01:48:37', 5, 299, 399, 'img/content/JobMixFormula/alfin47.jpg', 3),
(60, 0, 'Job Mix Formula 5', 4, 4, 200, 6, 200, 5, 200, '2021-07-14 01:53:08', 5, 299, 200, 'img/content/JobMixFormula/Screenshot_5.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `em_material`
--

CREATE TABLE `em_material` (
  `id_material` int(10) NOT NULL,
  `name_material` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_material`
--

INSERT INTO `em_material` (`id_material`, `name_material`, `status`) VALUES
(3, 'Additive', 0),
(4, 'Air', 0),
(7, 'Semen', 0),
(8, 'Halus', 0),
(9, 'Kasar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_mutu`
--

CREATE TABLE `em_mutu` (
  `id_mutu` int(10) NOT NULL,
  `name_mutu` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_mutu`
--

INSERT INTO `em_mutu` (`id_mutu`, `name_mutu`, `status`) VALUES
(4, 'K300', 0),
(5, 'K350', 0),
(6, 'K600', 0),
(7, 'K400', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_pelaksana`
--

CREATE TABLE `em_pelaksana` (
  `id_pelaksana` int(10) NOT NULL,
  `name_pelaksana` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_pelaksana`
--

INSERT INTO `em_pelaksana` (`id_pelaksana`, `name_pelaksana`, `status`) VALUES
(2, 'Alfin2', 0),
(4, 'Ivan Kolev', 0),
(5, 'Alfin Ganteng', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_product`
--

CREATE TABLE `em_product` (
  `id_product` int(10) NOT NULL,
  `name_product` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_product`
--

INSERT INTO `em_product` (`id_product`, `name_product`, `status`) VALUES
(0, 'Belum Terisi', 0),
(6, 'Paku Bumi Medium', 0),
(7, 'Beton Silinder', 0),
(8, 'Beton Precast', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_project`
--

CREATE TABLE `em_project` (
  `id_project` int(10) NOT NULL,
  `id_customer` int(10) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_project`
--

INSERT INTO `em_project` (`id_project`, `id_customer`, `project_name`, `status`) VALUES
(0, 0, 'Belum Terisi', 0),
(9, 10, 'Jalan Tol Aceh', 0),
(10, 20, 'Jalan Raya', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_quarry`
--

CREATE TABLE `em_quarry` (
  `id_quarry` int(10) NOT NULL,
  `name_quarry` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_quarry`
--

INSERT INTO `em_quarry` (`id_quarry`, `name_quarry`, `status`) VALUES
(1, 'gg', 0),
(3, 'Quarry Adhi Beton', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_sample_description`
--

CREATE TABLE `em_sample_description` (
  `id_sample_description` int(10) NOT NULL,
  `sample_description_name` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sample_description`
--

INSERT INTO `em_sample_description` (`id_sample_description`, `sample_description_name`, `status`) VALUES
(3, 'Kerikil rawan', 0),
(4, 'Batu dengan ukuran besar', 0),
(6, 'Kerikil sangat kuat', 0),
(7, 'Batu dengan ukuran besar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_sample_source`
--

CREATE TABLE `em_sample_source` (
  `id_sample_source` int(10) NOT NULL,
  `sample_source_name` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sample_source`
--

INSERT INTO `em_sample_source` (`id_sample_source`, `sample_source_name`, `status`) VALUES
(3, 'Gunung T', 0),
(5, 'Gunung Jati', 0),
(6, 'Gunung Bromo', 0),
(8, 'Gunung Cikuray', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_slump_flow_data`
--

CREATE TABLE `em_slump_flow_data` (
  `id_slump_flow_data` int(11) NOT NULL,
  `id_project` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_type` int(11) DEFAULT NULL,
  `admixture` varchar(100) NOT NULL,
  `id_job_mix_formula` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_mutu` int(11) NOT NULL,
  `suhu` varchar(100) NOT NULL,
  `doc_img` longtext NOT NULL,
  `id_status` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_slump_flow_data`
--

INSERT INTO `em_slump_flow_data` (`id_slump_flow_data`, `id_project`, `id_customer`, `id_type`, `admixture`, `id_job_mix_formula`, `id_product`, `id_mutu`, `suhu`, `doc_img`, `id_status`, `date_time`) VALUES
(89, 9, 10, 1, '100', 57, 7, 4, '50', 'img/content/SlumpFlowTest/alfin29.jpg', 1, '0000-00-00 00:00:00'),
(92, 9, 10, 1, '100', 58, 6, 4, '50', 'img/content/SlumpFlowTest/line_244234165199590.jpg', 1, '2021-07-11 16:50:55'),
(93, 0, 0, 0, '', 59, 6, 4, '', '', 3, '0000-00-00 00:00:00'),
(94, 0, 0, 0, '', 57, 7, 4, '', '', 3, '2021-07-14 01:43:02'),
(95, 0, 0, 0, '', 58, 6, 4, '', '', 3, '2021-07-14 01:43:11');

-- --------------------------------------------------------

--
-- Table structure for table `em_status`
--

CREATE TABLE `em_status` (
  `id_status` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_status`
--

INSERT INTO `em_status` (`id_status`, `status`) VALUES
(1, 'Diterima'),
(2, 'Ditolak'),
(3, 'Menunggu');

-- --------------------------------------------------------

--
-- Table structure for table `em_sumber_additive`
--

CREATE TABLE `em_sumber_additive` (
  `id_sumber_additive` int(11) NOT NULL,
  `tempat_sumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sumber_additive`
--

INSERT INTO `em_sumber_additive` (`id_sumber_additive`, `tempat_sumber`) VALUES
(5, 'Gunung Semeru'),
(6, 'Gunung Cikuray'),
(7, 'Gunung Lawu'),
(8, 'Gunung Argopuro');

-- --------------------------------------------------------

--
-- Table structure for table `em_sumber_halus`
--

CREATE TABLE `em_sumber_halus` (
  `id_sumber_halus` int(11) NOT NULL,
  `tempat_sumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sumber_halus`
--

INSERT INTO `em_sumber_halus` (`id_sumber_halus`, `tempat_sumber`) VALUES
(6, 'Gunung Semeru'),
(7, 'Gunung Cikuray'),
(8, 'Gunung Lawu'),
(9, 'Gunung Argopuro');

-- --------------------------------------------------------

--
-- Table structure for table `em_sumber_kasar`
--

CREATE TABLE `em_sumber_kasar` (
  `id_sumber_kasar` int(11) NOT NULL,
  `tempat_sumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sumber_kasar`
--

INSERT INTO `em_sumber_kasar` (`id_sumber_kasar`, `tempat_sumber`) VALUES
(5, 'Gunung Semeru'),
(6, 'Gunung Cikuray'),
(7, 'Gunung Lawu'),
(8, 'Gunung Argopuro');

-- --------------------------------------------------------

--
-- Table structure for table `em_sumber_semen`
--

CREATE TABLE `em_sumber_semen` (
  `id_sumber_semen` int(11) NOT NULL,
  `tempat_sumber` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_sumber_semen`
--

INSERT INTO `em_sumber_semen` (`id_sumber_semen`, `tempat_sumber`) VALUES
(4, 'Gunung Semeru'),
(5, 'Gunung Cikuray'),
(6, 'Gunung Lawu'),
(7, 'Gunung Argopuro');

-- --------------------------------------------------------

--
-- Table structure for table `em_supplier`
--

CREATE TABLE `em_supplier` (
  `id_supplier` int(10) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_supplier`
--

INSERT INTO `em_supplier` (`id_supplier`, `supplier_name`, `status`) VALUES
(1, 'Adhi Beton', 0),
(4, 'PT. Adhi Beton Jakarta', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_to_be_used`
--

CREATE TABLE `em_to_be_used` (
  `id_to_be_used` int(10) NOT NULL,
  `name_to_be_used` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_to_be_used`
--

INSERT INTO `em_to_be_used` (`id_to_be_used`, `name_to_be_used`, `status`) VALUES
(2, 'Alfin Rarr', 0),
(4, 'Alfin Rarry', 0),
(5, 'Ivan Kol', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_type`
--

CREATE TABLE `em_type` (
  `id_type` int(10) NOT NULL,
  `name_type` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_type`
--

INSERT INTO `em_type` (`id_type`, `name_type`, `status`) VALUES
(0, 'Belum Terisi\r\n', 0),
(1, 'Type 22', 0),
(4, 'Type 1', 0),
(5, 'Type 2', 0),
(6, 'Type 3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `em_useradmins`
--

CREATE TABLE `em_useradmins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_ban` int(1) NOT NULL DEFAULT 1,
  `id_role` int(10) UNSIGNED NOT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `em_useradmins`
--

INSERT INTO `em_useradmins` (`id`, `username`, `email`, `password`, `is_ban`, `id_role`, `active`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'wldnmkfi@gmail.com', '$2y$10$8or8jmv29.G1J.yj6AdHhuAJ0v3rRN0xdL7KBimYHqUOJBFoC9p5i', 1, 1, 0, '2019-03-27 20:33:48', '2019-07-08 02:21:07'),
(64, 'alfinrarry', 'alfinrarry@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 1, 0, '2019-07-16 00:46:21', NULL),
(119, 'alfinrarry99', 'cobaaja@gmail.com', '$2y$10$kPayWKHEu9LKnMsFo/I28.PTvtY3zlF7xz.qPSpLVEBLmy03AO0aC', 1, 1, 0, '2020-04-25 16:12:00', NULL),
(120, 'adminbaru', 'adminbaru@gmail.com', '$2y$10$USah9vPFpdqoQwg2oHQfb.Fr3D6y1N0LhWA6GnOqBLl.TvNxdTN.C', 1, 1, 0, '2020-04-30 21:33:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `em_useradmin_details`
--

CREATE TABLE `em_useradmin_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_useradmin` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `img_path` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `em_useradmin_details`
--

INSERT INTO `em_useradmin_details` (`id`, `id_useradmin`, `fullname`, `phone`, `created_at`, `updated_at`, `img_path`) VALUES
(1, 1, 'Admin Super', '', NULL, '2021-06-23 04:40:14', 'img/content/useradmin/3f1cd60569d9057268970fbe35905c04.png'),
(43, 64, 'Alfin Rizqiansi  Ansory Bin Ansory', '081232140049', '2019-07-16 00:46:21', '2020-04-30 21:38:17', 'img/content/useradmin/ee7f6989ee6eb429706fcc34d40e9b39.jpg'),
(89, 119, 'Alfin Rarry A', '12345678', '2020-04-25 16:12:00', '2020-04-26 13:19:30', 'img/content/useradmin/97e883fc4b3b6004d48ddae116de3d94.jpg'),
(90, 120, 'Admin Baru', '081232140049', '2020-04-30 21:33:51', NULL, 'img/content/useradmin/b4db961e98a86bf75c0c6f0e6b48ee1b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `em_useradmin_roles`
--

CREATE TABLE `em_useradmin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `em_useradmin_roles`
--

INSERT INTO `em_useradmin_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ketua Madrasah', '2019-02-17 23:29:13', '2019-02-24 12:23:13'),
(2, 'Kurikulum Madrasah', '2019-02-21 18:28:13', '2018-12-06 19:45:13'),
(3, 'Ustadz Madrasah', '2018-12-15 10:43:13', '2018-12-04 13:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `em_vendor`
--

CREATE TABLE `em_vendor` (
  `id_vendor` int(10) NOT NULL,
  `vendor_name` varchar(100) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `em_vendor`
--

INSERT INTO `em_vendor` (`id_vendor`, `vendor_name`, `status`) VALUES
(1, 'Hutama Karya Jakarta', 0),
(4, 'PT. Hutama Karya Jakarta', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `em_archive_test_files`
--
ALTER TABLE `em_archive_test_files`
  ADD PRIMARY KEY (`id_archive_test`),
  ADD KEY `em_archive_test_files_ibfk_1` (`id_sample_source`),
  ADD KEY `em_archive_test_files_ibfk_2` (`id_pelaksana`);

--
-- Indexes for table `em_cache_slump`
--
ALTER TABLE `em_cache_slump`
  ADD PRIMARY KEY (`id_cache`),
  ADD KEY `id_job_mix_formula` (`id_job_mix_formula`);

--
-- Indexes for table `em_customer`
--
ALTER TABLE `em_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `em_daily_compressive`
--
ALTER TABLE `em_daily_compressive`
  ADD PRIMARY KEY (`id_daily_compressive`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_project` (`id_project`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_mutu` (`id_mutu`),
  ADD KEY `id_type` (`id_type`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_job_mix_formula` (`id_job_mix_formula`);

--
-- Indexes for table `em_daily_incoming_additive`
--
ALTER TABLE `em_daily_incoming_additive`
  ADD PRIMARY KEY (`id_daily_incoming_additive`),
  ADD KEY `em_daily_incoming_additive_ibfk_5` (`id_supplier`),
  ADD KEY `em_daily_incoming_additive_ibfk_6` (`id_pelaksana`);

--
-- Indexes for table `em_daily_incoming_halus`
--
ALTER TABLE `em_daily_incoming_halus`
  ADD PRIMARY KEY (`id_daily_incoming_halus`),
  ADD KEY `em_daily_incoming_halus_ibfk_1` (`id_material`),
  ADD KEY `em_daily_incoming_halus_ibfk_2` (`id_vendor`),
  ADD KEY `em_daily_incoming_halus_ibfk_3` (`id_quarry`),
  ADD KEY `em_daily_incoming_halus_ibfk_4` (`id_pelaksana`);

--
-- Indexes for table `em_daily_incoming_kasar`
--
ALTER TABLE `em_daily_incoming_kasar`
  ADD PRIMARY KEY (`id_daily_incoming_kasar`),
  ADD KEY `em_daily_incoming_kasar_ibfk_1` (`id_material`),
  ADD KEY `em_daily_incoming_kasar_ibfk_2` (`id_vendor`),
  ADD KEY `em_daily_incoming_kasar_ibfk_3` (`id_quarry`),
  ADD KEY `em_daily_incoming_kasar_ibfk_4` (`id_pelaksana`);

--
-- Indexes for table `em_independent_test`
--
ALTER TABLE `em_independent_test`
  ADD PRIMARY KEY (`id_independent_test`),
  ADD KEY `em_independent_test_ibfk_1` (`id_material`),
  ADD KEY `em_independent_test_ibfk_2` (`id_sample_source`),
  ADD KEY `em_independent_test_ibfk_3` (`id_pelaksana`);

--
-- Indexes for table `em_internal_tes_halus`
--
ALTER TABLE `em_internal_tes_halus`
  ADD PRIMARY KEY (`id_internal_tes_halus`),
  ADD KEY `em_internal_tes_halus_ibfk_1` (`id_sample_source`),
  ADD KEY `em_internal_tes_halus_ibfk_2` (`id_sample_description`),
  ADD KEY `em_internal_tes_halus_ibfk_3` (`id_pelaksana`),
  ADD KEY `em_internal_tes_halus_ibfk_4` (`id_to_be_used`);

--
-- Indexes for table `em_internal_tes_kasar`
--
ALTER TABLE `em_internal_tes_kasar`
  ADD PRIMARY KEY (`id_internal_tes_kasar`),
  ADD KEY `em_internal_tes_kasar_ibfk_1` (`id_sample_source`),
  ADD KEY `em_internal_tes_kasar_ibfk_2` (`id_sample_description`),
  ADD KEY `em_internal_tes_kasar_ibfk_3` (`id_pelaksana`),
  ADD KEY `em_internal_tes_kasar_ibfk_4` (`id_to_be_used`);

--
-- Indexes for table `em_job_mix_formula`
--
ALTER TABLE `em_job_mix_formula`
  ADD PRIMARY KEY (`id_job_mix_formula`),
  ADD KEY `em_job_mix_formula_ibfk_1` (`id_product`),
  ADD KEY `em_job_mix_formula_ibfk_2` (`id_mutu`);

--
-- Indexes for table `em_material`
--
ALTER TABLE `em_material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indexes for table `em_mutu`
--
ALTER TABLE `em_mutu`
  ADD PRIMARY KEY (`id_mutu`);

--
-- Indexes for table `em_pelaksana`
--
ALTER TABLE `em_pelaksana`
  ADD PRIMARY KEY (`id_pelaksana`);

--
-- Indexes for table `em_product`
--
ALTER TABLE `em_product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `em_project`
--
ALTER TABLE `em_project`
  ADD PRIMARY KEY (`id_project`),
  ADD KEY `em_project_ibfk_1` (`id_customer`);

--
-- Indexes for table `em_quarry`
--
ALTER TABLE `em_quarry`
  ADD PRIMARY KEY (`id_quarry`);

--
-- Indexes for table `em_sample_description`
--
ALTER TABLE `em_sample_description`
  ADD PRIMARY KEY (`id_sample_description`);

--
-- Indexes for table `em_sample_source`
--
ALTER TABLE `em_sample_source`
  ADD PRIMARY KEY (`id_sample_source`);

--
-- Indexes for table `em_slump_flow_data`
--
ALTER TABLE `em_slump_flow_data`
  ADD PRIMARY KEY (`id_slump_flow_data`),
  ADD KEY `id_project` (`id_project`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_job_mix_formula` (`id_job_mix_formula`);

--
-- Indexes for table `em_status`
--
ALTER TABLE `em_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `em_sumber_additive`
--
ALTER TABLE `em_sumber_additive`
  ADD PRIMARY KEY (`id_sumber_additive`);

--
-- Indexes for table `em_sumber_halus`
--
ALTER TABLE `em_sumber_halus`
  ADD PRIMARY KEY (`id_sumber_halus`);

--
-- Indexes for table `em_sumber_kasar`
--
ALTER TABLE `em_sumber_kasar`
  ADD PRIMARY KEY (`id_sumber_kasar`);

--
-- Indexes for table `em_sumber_semen`
--
ALTER TABLE `em_sumber_semen`
  ADD PRIMARY KEY (`id_sumber_semen`);

--
-- Indexes for table `em_supplier`
--
ALTER TABLE `em_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `em_to_be_used`
--
ALTER TABLE `em_to_be_used`
  ADD PRIMARY KEY (`id_to_be_used`);

--
-- Indexes for table `em_type`
--
ALTER TABLE `em_type`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `em_useradmins`
--
ALTER TABLE `em_useradmins`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `em_users_email_unique` (`email`) USING BTREE,
  ADD KEY `em_users_id_role_foreign` (`id_role`) USING BTREE;

--
-- Indexes for table `em_useradmin_details`
--
ALTER TABLE `em_useradmin_details`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `em_user_details_id_user_foreign` (`id_useradmin`) USING BTREE;

--
-- Indexes for table `em_useradmin_roles`
--
ALTER TABLE `em_useradmin_roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `em_vendor`
--
ALTER TABLE `em_vendor`
  ADD PRIMARY KEY (`id_vendor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `em_archive_test_files`
--
ALTER TABLE `em_archive_test_files`
  MODIFY `id_archive_test` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `em_cache_slump`
--
ALTER TABLE `em_cache_slump`
  MODIFY `id_cache` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `em_customer`
--
ALTER TABLE `em_customer`
  MODIFY `id_customer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `em_daily_compressive`
--
ALTER TABLE `em_daily_compressive`
  MODIFY `id_daily_compressive` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `em_daily_incoming_additive`
--
ALTER TABLE `em_daily_incoming_additive`
  MODIFY `id_daily_incoming_additive` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `em_daily_incoming_halus`
--
ALTER TABLE `em_daily_incoming_halus`
  MODIFY `id_daily_incoming_halus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `em_daily_incoming_kasar`
--
ALTER TABLE `em_daily_incoming_kasar`
  MODIFY `id_daily_incoming_kasar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `em_independent_test`
--
ALTER TABLE `em_independent_test`
  MODIFY `id_independent_test` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `em_internal_tes_halus`
--
ALTER TABLE `em_internal_tes_halus`
  MODIFY `id_internal_tes_halus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `em_internal_tes_kasar`
--
ALTER TABLE `em_internal_tes_kasar`
  MODIFY `id_internal_tes_kasar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `em_job_mix_formula`
--
ALTER TABLE `em_job_mix_formula`
  MODIFY `id_job_mix_formula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `em_material`
--
ALTER TABLE `em_material`
  MODIFY `id_material` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `em_mutu`
--
ALTER TABLE `em_mutu`
  MODIFY `id_mutu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `em_pelaksana`
--
ALTER TABLE `em_pelaksana`
  MODIFY `id_pelaksana` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `em_product`
--
ALTER TABLE `em_product`
  MODIFY `id_product` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `em_project`
--
ALTER TABLE `em_project`
  MODIFY `id_project` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `em_quarry`
--
ALTER TABLE `em_quarry`
  MODIFY `id_quarry` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `em_sample_description`
--
ALTER TABLE `em_sample_description`
  MODIFY `id_sample_description` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `em_sample_source`
--
ALTER TABLE `em_sample_source`
  MODIFY `id_sample_source` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `em_slump_flow_data`
--
ALTER TABLE `em_slump_flow_data`
  MODIFY `id_slump_flow_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `em_status`
--
ALTER TABLE `em_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `em_sumber_additive`
--
ALTER TABLE `em_sumber_additive`
  MODIFY `id_sumber_additive` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `em_sumber_halus`
--
ALTER TABLE `em_sumber_halus`
  MODIFY `id_sumber_halus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `em_sumber_kasar`
--
ALTER TABLE `em_sumber_kasar`
  MODIFY `id_sumber_kasar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `em_sumber_semen`
--
ALTER TABLE `em_sumber_semen`
  MODIFY `id_sumber_semen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `em_supplier`
--
ALTER TABLE `em_supplier`
  MODIFY `id_supplier` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `em_to_be_used`
--
ALTER TABLE `em_to_be_used`
  MODIFY `id_to_be_used` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `em_type`
--
ALTER TABLE `em_type`
  MODIFY `id_type` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `em_useradmins`
--
ALTER TABLE `em_useradmins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `em_useradmin_details`
--
ALTER TABLE `em_useradmin_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `em_useradmin_roles`
--
ALTER TABLE `em_useradmin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `em_vendor`
--
ALTER TABLE `em_vendor`
  MODIFY `id_vendor` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `em_archive_test_files`
--
ALTER TABLE `em_archive_test_files`
  ADD CONSTRAINT `em_archive_test_files_ibfk_1` FOREIGN KEY (`id_sample_source`) REFERENCES `em_sample_source` (`id_sample_source`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_archive_test_files_ibfk_2` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE;

--
-- Constraints for table `em_cache_slump`
--
ALTER TABLE `em_cache_slump`
  ADD CONSTRAINT `job_mix_formula_1` FOREIGN KEY (`id_job_mix_formula`) REFERENCES `em_job_mix_formula` (`id_job_mix_formula`) ON DELETE CASCADE;

--
-- Constraints for table `em_daily_incoming_additive`
--
ALTER TABLE `em_daily_incoming_additive`
  ADD CONSTRAINT `em_daily_incoming_additive_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `em_supplier` (`id_supplier`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_additive_ibfk_3` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_additive_ibfk_5` FOREIGN KEY (`id_supplier`) REFERENCES `em_supplier` (`id_supplier`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_additive_ibfk_6` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE;

--
-- Constraints for table `em_daily_incoming_halus`
--
ALTER TABLE `em_daily_incoming_halus`
  ADD CONSTRAINT `em_daily_incoming_halus_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `em_material` (`id_material`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_halus_ibfk_2` FOREIGN KEY (`id_vendor`) REFERENCES `em_vendor` (`id_vendor`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_halus_ibfk_3` FOREIGN KEY (`id_quarry`) REFERENCES `em_quarry` (`id_quarry`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_halus_ibfk_4` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE;

--
-- Constraints for table `em_daily_incoming_kasar`
--
ALTER TABLE `em_daily_incoming_kasar`
  ADD CONSTRAINT `em_daily_incoming_kasar_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `em_material` (`id_material`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_kasar_ibfk_2` FOREIGN KEY (`id_vendor`) REFERENCES `em_vendor` (`id_vendor`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_kasar_ibfk_3` FOREIGN KEY (`id_quarry`) REFERENCES `em_quarry` (`id_quarry`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_daily_incoming_kasar_ibfk_4` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE;

--
-- Constraints for table `em_independent_test`
--
ALTER TABLE `em_independent_test`
  ADD CONSTRAINT `em_independent_test_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `em_material` (`id_material`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_independent_test_ibfk_2` FOREIGN KEY (`id_sample_source`) REFERENCES `em_sample_source` (`id_sample_source`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_independent_test_ibfk_3` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE;

--
-- Constraints for table `em_internal_tes_halus`
--
ALTER TABLE `em_internal_tes_halus`
  ADD CONSTRAINT `em_internal_tes_halus_ibfk_1` FOREIGN KEY (`id_sample_source`) REFERENCES `em_sample_source` (`id_sample_source`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_halus_ibfk_2` FOREIGN KEY (`id_sample_description`) REFERENCES `em_sample_description` (`id_sample_description`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_halus_ibfk_3` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_halus_ibfk_4` FOREIGN KEY (`id_to_be_used`) REFERENCES `em_to_be_used` (`id_to_be_used`) ON DELETE CASCADE;

--
-- Constraints for table `em_internal_tes_kasar`
--
ALTER TABLE `em_internal_tes_kasar`
  ADD CONSTRAINT `em_internal_tes_kasar_ibfk_1` FOREIGN KEY (`id_sample_source`) REFERENCES `em_sample_source` (`id_sample_source`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_kasar_ibfk_2` FOREIGN KEY (`id_sample_description`) REFERENCES `em_sample_description` (`id_sample_description`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_kasar_ibfk_3` FOREIGN KEY (`id_pelaksana`) REFERENCES `em_pelaksana` (`id_pelaksana`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_internal_tes_kasar_ibfk_4` FOREIGN KEY (`id_to_be_used`) REFERENCES `em_to_be_used` (`id_to_be_used`) ON DELETE CASCADE;

--
-- Constraints for table `em_job_mix_formula`
--
ALTER TABLE `em_job_mix_formula`
  ADD CONSTRAINT `em_job_mix_formula_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `em_product` (`id_product`) ON DELETE CASCADE,
  ADD CONSTRAINT `em_job_mix_formula_ibfk_2` FOREIGN KEY (`id_mutu`) REFERENCES `em_mutu` (`id_mutu`) ON DELETE CASCADE;

--
-- Constraints for table `em_project`
--
ALTER TABLE `em_project`
  ADD CONSTRAINT `em_project_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `em_customer` (`id_customer`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
