<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SumberMaterialKasar extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SumberMaterial','SumberMaterial');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Sumber Material Kasar
   public function listSumberMaterialKasar(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Sumber Material Kasar';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialKasar()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SumberMaterialKasar/listSumberMaterialKasar');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sumberMaterialKasarList',$data);
  }
    // End List  Sumber Material Kasar

 // Add Sumber Material Kasar
 public function inputSumberMaterialKasar(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sumber Material Kasar';
 {
    $data_sumber_material = array(
    
      'tempat_sumber'            => $_POST['tempat_sumber']
    );
    $data = $this->SumberMaterial->inputSumberMaterialKasar($data_sumber_material);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SumberMaterialKasar/listSumberMaterialKasar"));

}
// End Add Sumber Material Kasar

    // Form Sumber Material Kasar
    public function formSumberMaterialKasar(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sumber Material Halus';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SumberMaterialKasar/formSumberMaterialKasar');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sumberMaterialKasarForm',$data);
    }
    // End Form Sumber Material Kasar

      // Form Edit Sumber Material Kasar
      public function formEditSumberMaterialKasar($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Sumber Material Kasar';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialKasarById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SumberMaterialKasar/formEditSumberMaterialKasar');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sumberMaterialKasarEditForm',$data);
      }
      // End Form Edit SumberMaterial Kasar

       // Update Sumber Material Kasar
    public function updateSumberMaterialKasar($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Kasar';
      $data['sumberMaterial']        = $this->SumberMaterial->getSumberMaterialKasarById($id)->row_array();

     {
        $data_sumber_material = array(
        
            'tempat_sumber'     => $_POST['tempat_sumber']
        );
        $data = $this->SumberMaterial->updateSumberMaterialKasar($id,$data_sumber_material);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SumberMaterialKasar/listSumberMaterialKasar"));

    }

        // End Update Sumber Material Kasar

  
//   Delete Data Sumber Material Kasar
  public function delSumberMaterialKasar($id){
    $data= $this->SumberMaterial->delSumberMaterialKasar($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SumberMaterialKasar/listSumberMaterialKasar"));
  }
//   Delete Data Sumber Material Kasar
}

/* End of file User.php */
