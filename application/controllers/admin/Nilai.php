<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_nilai','nilai');
    $this->load->library('email');
    $this->load->library('session');
    
  }
  
  public function cariSantri(){
    $data['codepage'] = "back_useradmin";
    $data['page_title'] 	= 'Cari Santri';
    $data['santri']        = $this->santri->getListSantri()->result_array();
    $data['kelas']         = $this->santri->getListKelas()->result_array();
    $id = $_SESSION['id'];
    $data['image']    = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/listNilai/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    
    $this->template->back_views('site/back/nilaiCari',$data);
    
  }

 

  public function listNilai(){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= 'Cari Nilai Santri';
    $data['santri']       = $this->santri->getListSantri()->result_array();
    $data['kelas']        = $this->santri->getListKelas()->result_array();
    $id = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/listNilai/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
  
    $this->template->back_views('site/back/nilaiList',$data);
    
  }

  public function detailNilai($nis=0){
    $data['codepage']       = "back_useradmin";
    $data['page_title'] 	  = "Detail Nilai";
    $data['nilai']          = $this->nilai->getNilaiByNis($nis)->row_array();
    $data['img']            = $this->user->getImageUserSantri($nis)->row_array();
    $id                     = $_SESSION['id'];
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['komplek']        = $this->santri->getListKomplek()->result_array();
    $data['kelas']          = $this->santri->getListKelas()->result_array();
    $data['th']             = $this->nilai->getTahunAjaran()->result_array();
    $data['smt']            = $this->nilai->getSemester()->result_array();

    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/detailNilai/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $data['rekap']          = $this->nilai->getRekapById($id)->row_array();
    $nilai                  = $this->nilai->getNilaiByNis($nis)->row_array();
    $rekap = $nilai['id_nilai'];
    $rekap++;
    $nis_nilai              = $this->nilai->getNilaiByNis($nis)->row_array();
    $nis= $nis_nilai['nis'];
    $kelas=$nis_nilai['id_kelas'];
  
    if(isset($_POST['submit'])){

        $data = array(
        
         
        'nis'                 => @$nis,
        'id_kelas'            => @$kelas,
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_tajwid'            => $_POST['n_tajwid'] ,
        'n_alquran'           => $_POST['n_alquran'] ,
        'n_imla'              => $_POST['n_imla'] ,
        'n_tarikh'            => $_POST['n_tarikh'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
          
        );
        $data = $this->nilai->addRekapNilaiSantri($data);
      
      $data = array(
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_tajwid'            => $_POST['n_tajwid'] ,
        'n_alquran'           => $_POST['n_alquran'] ,
        'n_imla'              => $_POST['n_imla'] ,
        'n_tarikh'            => $_POST['n_tarikh'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
 
      );
      $data = $this->nilai->updateNilaiSantri($nis,$data);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan, silahkan cek di rekap nilai !');  
      redirect(base_url('admin/nilai/pencarian/?kelas=1'));
    }
    
  
  $this->template->back_views('site/back/nilaiDetail',$data);
    
  }

  public function detailNilaiUla($nis=0){
    $data['codepage']       = "back_useradmin";
    $data['page_title'] 	  = "Detail Nilai";
    $data['nilai']          = $this->nilai->getNilaiByNis($nis)->row_array();
    $data['img']            = $this->user->getImageUserSantri($nis)->row_array();
    $id                     = $_SESSION['id'];
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['komplek']        = $this->santri->getListKomplek()->result_array();
    $data['kelas']          = $this->santri->getListKelas()->result_array();
    $data['th']             = $this->nilai->getTahunAjaran()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/detailNilai/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $data['smt']            = $this->nilai->getSemester()->result_array();
    $data['rekap']          = $this->nilai->getRekapById($id)->row_array();
    $nilai                  = $this->nilai->getNilaiByNis($nis)->row_array();
    $rekap = $nilai['id_nilai'];
    $rekap++;
    $nis_nilai              = $this->nilai->getNilaiByNis($nis)->row_array();
    $nis= $nis_nilai['nis'];
    $kelas=$nis_nilai['id_kelas'];
  
    if(isset($_POST['submit'])){

      $data = array(
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_tajwid'            => $_POST['n_tajwid'] ,
        'n_alquran'           => $_POST['n_alquran'] ,
        'n_imla'              => $_POST['n_imla'] ,
        'n_tarikh'            => $_POST['n_tarikh'] ,
        'n_shorof'            => $_POST['n_shorof'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
 
      );
      $data = $this->nilai->updateNilaiSantri($nis,$data);

      $data = array(
          'nis'                 => @$nis,
          'id_kelas'            => @$kelas,
          'semester'            => $_POST['semester'] ,
          'n_tauhid'            => $_POST['n_tauhid'],
          'n_fiqih'             => $_POST['n_fiqih'] ,
          'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
          'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
          'n_tajwid'            => $_POST['n_tajwid'] ,
          'n_alquran'           => $_POST['n_alquran'] ,
          'n_imla'              => $_POST['n_imla'] ,
          'n_tarikh'            => $_POST['n_tarikh'] ,
          'n_shorof'            => $_POST['n_shorof'] ,
          'tahun_ajaran'        => $_POST['tahun_ajaran'] 
   
        );
        $data = $this->nilai->addRekapNilaiSantri($data);
        $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan, silahkan cek di rekap nilai !');  
        redirect($_SERVER['HTTP_REFERER']);
    
  }
  $this->template->back_views('site/back/nilaiDetailUla',$data);
    
  }

  public function detailNilaiWustho($nis=0){
    $data['codepage']       = "back_useradmin";
    $data['page_title'] 	  = "Detail Nilai";
    $data['nilai']          = $this->nilai->getNilaiByNis($nis)->row_array();
    $data['img']            = $this->user->getImageUserSantri($nis)->row_array();
    $id                     = $_SESSION['id'];
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['komplek']        = $this->santri->getListKomplek()->result_array();
    $data['kelas']          = $this->santri->getListKelas()->result_array();
    $data['th']             = $this->nilai->getTahunAjaran()->result_array();
    $data['smt']            = $this->nilai->getSemester()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/detailNilai/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }


    $data['rekap']          = $this->nilai->getRekapById($id)->row_array();
    $nilai                  = $this->nilai->getNilaiByNis($nis)->row_array();
    $nis_nilai              = $this->nilai->getNilaiByNis($nis)->row_array();
    $nis= $nis_nilai['nis'];
    $kelas=$nis_nilai['id_kelas'];
  
    if(isset($_POST['submit'])){

      $data = array(
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_hadits'            => $_POST['n_hadits'] ,
        'n_b_arab'            => $_POST['n_b_arab'] ,
        'n_tafsir'            => $_POST['n_tafsir'] ,
        'n_faroidh'           => $_POST['n_faroidh'] ,
        'n_nahwu'             => $_POST['n_nahwu'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
 
      );
      $data = $this->nilai->updateNilaiSantri($nis,$data);

      $data = array(
          'nis'                 => @$nis,
          'id_kelas'            => @$kelas,
          'semester'            => $_POST['semester'] ,
          'n_tauhid'            => $_POST['n_tauhid'],
          'n_fiqih'             => $_POST['n_fiqih'] ,
          'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
          'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
          'n_hadits'            => $_POST['n_hadits'] ,
          'n_b_arab'            => $_POST['n_b_arab'] ,
          'n_tafsir'            => $_POST['n_tafsir'] ,
          'n_faroidh'           => $_POST['n_faroidh'] ,
          'n_nahwu'             => $_POST['n_nahwu'] ,
          'tahun_ajaran'        => $_POST['tahun_ajaran'] 
   
        );
        $data = $this->nilai->addRekapNilaiSantri($data);
        $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan, silahkan cek di rekap nilai !');  
        redirect($_SERVER['HTTP_REFERER']);
    
  }
  $this->template->back_views('site/back/nilaiDetailWustho',$data);
    
  }

  public function detailNilaiUlya($nis=0){
    $data['codepage']       = "back_useradmin";
    $data['page_title'] 	  = "Detail Nilai";
    $data['nilai']          = $this->nilai->getNilaiByNis($nis)->row_array();
    $data['img']            = $this->user->getImageUserSantri($nis)->row_array();
    $id                     = $_SESSION['id'];
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['komplek']        = $this->santri->getListKomplek()->result_array();
    $data['kelas']          = $this->santri->getListKelas()->result_array();
    $data['th']             = $this->nilai->getTahunAjaran()->result_array();
    $data['smt']            = $this->nilai->getSemester()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/detailNilai/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $data['rekap']          = $this->nilai->getRekapById($id)->row_array();
    $nilai                  = $this->nilai->getNilaiByNis($nis)->row_array();
    $rekap = $nilai['id_nilai'];
    $rekap++;
    $nis_nilai              = $this->nilai->getNilaiByNis($nis)->row_array();
    $nis                    = $nis_nilai['nis'];
    $kelas                  = $nis_nilai['id_kelas'];
   
    if(isset($_POST['submit'])){

      $data = array(
        'nis'                 => @$nis,
        'id_kelas'            => @$kelas,
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_hadits'            => $_POST['n_hadits'] ,
        'n_nahwu'             => $_POST['n_nahwu'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
  
      );
      $data = $this->nilai->addRekapNilaiSantri($data);

      $data = array(
        'semester'            => $_POST['semester'] ,
        'n_tauhid'            => $_POST['n_tauhid'],
        'n_fiqih'             => $_POST['n_fiqih'] ,
        'n_mukhofadhoh'       => $_POST['n_mukhofadhoh'] ,
        'n_qiroatul_kitab'    => $_POST['n_qiroatul_kitab'] ,
        'n_hadits'            => $_POST['n_hadits'] ,
        'n_nahwu'             => $_POST['n_nahwu'] ,
        'tahun_ajaran'        => $_POST['tahun_ajaran'] 
 
      );
      $data = $this->nilai->updateNilaiSantri($nis,$data);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan, silahkan cek di rekap nilai !');  
      redirect($_SERVER['HTTP_REFERER']);
    
  }
  $this->template->back_views('site/back/nilaiDetailUlya',$data);
    
  }

  

public function pencarian()
{
  $data['codepage']      = "back_useradmin";
  $data['page_title'] 	 = "Riwayat Terkahir Input Nilai Santri";
  $id                    = $_SESSION['id'];
  $data['image']         = $this->user->getImage($id)->result_array();
  $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
  $data['santri']        = $this->santri->getListSantri()->result_array();
  $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
  $data['nilaiAll']      = $this->nilai->getListNilai()->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/Nilai/listNilai');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }


  
  $kelas =18;
  $kelas=$this->input->get('kelas');
  $data['hasil'] = $this->nilai->pencarian_santri($kelas)->result_array();

        
  if ($kelas <= 1 )
  {        
  $this->template->back_views('site/back/nilaiCariKelas',$data);
  }
  elseif ($kelas <=8)
  {
    $this->template->back_views('site/back/nilaiCariKelasUla',$data);
  }
  elseif ($kelas <=13)
  {
    $this->template->back_views('site/back/nilaiCariKelasWustho',$data);
  }
  elseif ($kelas >=14)
  {
    $this->template->back_views('site/back/nilaiCariKelasUlya',$data);
  }

}


  public function rekap_nilai()
  {
    $data['codepage']      = "back_useradmin";
    $data['page_title'] 	 = "Hasil Pencarian  Santri";
    $id                    = $_SESSION['id'];
    $data['image']         = $this->user->getImage($id)->result_array();
    $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
    $data['santri']        = $this->santri->getListSantri()->result_array();
    $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
    $data['rekap']         = $this->nilai->getAllRekap()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Nilai/listNilai');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }


  
    $kelas=$this->input->get('kelas');
    $data['hasil'] = $this->nilai->pencarian_kelas($kelas)->result_array();
    $this->template->back_views('site/back/nilaiCariSantri',$data);

  }  


}

/* End of file User.php */
