<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class IndependentTest extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_IndependentTest','IndependentTest');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List Agregat IndependentTest
  public function listIndependentTest(){
    $data['codepage']         = "back_addProduct";
    $data['page_title']   	  = ' List Independent Test';
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();
    $data['independentTest']  = $this->IndependentTest->getIndependentTest()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/InternalTestKasar/listAgregatKasar');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/independentTestList',$data);
  }
    // List Agregat IndependentTest

    // Form Agregat IndependentTest
    public function formIndependentTest(){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	   = 'Form Add Independent Test';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['sampleSource']        = $this->IndependentTest->getSampleSource()->result_array();
      $data['sampleDescription']   = $this->IndependentTest->getSampleDescription()->result_array();
      $data['pelaksana']           = $this->IndependentTest->getPelaksana()->result_array();
      $data['material']            = $this->IndependentTest->getMaterial()->result_array();
      $data['toBeUsed']            = $this->IndependentTest->getToBeUsed()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/IndependentTest/formIndependentTest');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/independentTestForm',$data);
    }
    // Form Agregat IndependentTest

    // Add Agregat IndependentTest
    public function inputIndependentTest(){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Add Independent Test';
      $data['independentTest']  = $this->IndependentTest->getIndependentTest()->result_array();
  
      {
        $config['upload_path']    ='./assets/img/content/IndependentTest/';
        $config['allowed_types']  ='jpg|png|ico|pdf|docx';
  
        $this->load->library('upload',$config);
        if($this->upload->do_upload('doc_img'))
        {
            $img1 ='img/content/IndependentTest/';
            $img1.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('sertif_img'))
        {
            $img2 ='img/content/IndependentTest/';
            $img2.=  $this->upload->data('file_name');
        }
       
        $status='3';
        $data_independent_test = array(
        
          'id_material'             => $_POST['id_material'] ,
          'id_sample_source'        => $_POST['id_sample_source'] ,
          'id_sample_description'   => $_POST['id_sample_description'],
          'id_pelaksana'            => $_POST['id_pelaksana'] ,
          'received_date'           => date('Y-m-j'),
          'doc_img'                 => @$img1,
          'sertif_img'              => @$img2,
          'id_status'               => @$status
        );
        $data = $this->IndependentTest->inputIndependentTest($data_independent_test);
         
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url("admin/IndependentTest/listIndependentTest"));    }

        // End Add IndependentTest

        // Edit dan View  IndependentTest
    public function detailIndependentTest($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Independent Test";
      $data['independentTest']       = $this->IndependentTest->getIndependentTestById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/IndependentTest/detailIndependentTest/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/independentTestDetail',$data);

    }

    // End Edit dan View IndependentTest


  // Tolak Incoming IndependentTest
public function tolakIndependentTest($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Tolak Independent Test';
  $data['independentTest']     = $this->IndependentTest->getIndependentTestById($id)->row_array();

  $status='2';
 {
    $data_independent_test = array(
    
        'id_status'                => @$status
    );

    $data = $this->IndependentTest->tolakIndependentTest($id,$data_independent_test);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
  redirect(base_url("admin/IndependentTest/listIndependentTest"));

}
// End Tolak IndependentTest

// Setuju InternalTesIndependentTesttKasar
public function setujuIndependentTest($id=0){
  $data['codepage']              = "back_addProduct";
  $data['page_title'] 	         = 'Setuju Independent Test';
  $data['independentTest']       = $this->IndependentTest->getIndependentTestById($id)->row_array();

  $status='1';
 {
    $data_independent_test = array(
    
        'id_status'                => @$status
    );

    $data = $this->IndependentTest->setujuIndependentTest($id,$data_independent_test);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
  redirect(base_url("admin/IndependentTest/listIndependentTest"));

}
// End Setuju InternalTestKasar

public function delIndependentTest($id){
  $data= $this->IndependentTest->delIndependentTest($id);
   $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
   redirect(base_url("admin/IndependentTest/listIndependentTest"));
}

}

/* End of file User.php */
