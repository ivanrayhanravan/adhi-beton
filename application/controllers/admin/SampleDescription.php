<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SampleDescription extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SampleDescription','SampleDescription');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Sample Description
   public function listSampleDescription(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Sample Description';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['sampleDescription']   = $this->SampleDescription->getSampleDescription()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SampleDescription/listSampleDescription');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sampleDescriptionList',$data);
  }
    // End List Sample Description

 // Add Sample Description
 public function inputSampleDescription(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sample Description';
 {
    $data_sample_description = array(
    
      'sample_description_name'   => $_POST['sample_description_name']
    );
    $data = $this->SampleDescription->inputSampleDescription($data_sample_description);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SampleDescription/listSampleDescription"));

}
// End Add Sample Description

    // Form Sample Description
    public function formSampleDescription(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sample Description';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SampleDescription/formSampleDescription');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sampleDescriptionForm',$data);
    }
    // End Form Sample Description   

      // Form Edit Sample Description
      public function formEditSampleDescription($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Sample Description';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['sampleDescription']   = $this->SampleDescription->getSampleDescriptionById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SampleDescription/formEditSampleDescription');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sampleDescriptionEditForm',$data);
      }
      // End Form Edit Sample Description

       // Update Sample Description
    public function updateSampleDescription($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Sample Description';
      $data['sampleDescription']     = $this->SampleDescription->getSampleDescriptionById($id)->row_array();

     {
        $data_sample_description = array(
        
          'sample_description_name'  => $_POST['sample_description_name']
        );
        $data = $this->SampleDescription->updateSampleDescription($id,$data_sample_description);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SampleDescription/listSampleDescription"));

    }

        // End Update Sample Description

  
//   Delete Data Sample Description
  public function delSampleDescription($id){
    $data= $this->SampleDescription->delSampleDescription($id);
           $this->SampleDescription->delSampleDescription($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SampleDescription/listSampleDescription"));
  }
//   Delete Data Sample Description

}

/* End of file User.php */
