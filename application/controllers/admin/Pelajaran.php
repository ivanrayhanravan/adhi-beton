<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelajaran extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_pelajaran','pelajaran');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  public function create_pelajaran(){
    $data['codepage'] = "back_addProduct";
    $data['page_title'] 	= 'Add Pelajaran';
    if(isset($_POST['submit'])){
     
  
      $data_pelajaran = array(
        'nama_pelajaran'   => $_POST['nama_pelajaran'] ,
        'created_at'       => date('Y-m-j ')
      );
      
      $data = $this->pelajaran->create_pelajaran($data_pelajaran);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/pelajaran/listPelajaran'));
    }
    
  }

  public function formAddPelajaran(){
    $data['codepage']         = "back_addProduct";
    $data['page_title'] 	    = 'Tambah Pelajaran';
    $data['userAdminRole']    = $this->user->getAllRole()->result_array();
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Pelajaran/formAddPelajaran');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/pelajaranAdd',$data);
  }

 

  public function listPelajaran(){
    $data['codepage']     = "back_user";
    $data['page_title'] 	= 'List Data Pelajaran';
    $data['user']         = $this->user->getListUser()->result_array();
    $data['pelajaran']    = $this->pelajaran->getListPelajaran()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Pelajaran/listPelajaran');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }


    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Pelajaran/listPelajaran/');
  
    } else { 
      redirect(base_url('login_admin'));
    }   
    $this->template->back_views('site/back/pelajaranList',$data);
  }

 

  public function detailPelajaran($id=0){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= "Detail Pelajaran";
    $data['pelajaran']    = $this->pelajaran->getPelajaranById($id)->row_array();
    $data['useradmin']    = $this->user->getUserAdminById($id)->row_array();
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Pelajaran/detailPelajran/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    if(isset($_POST['submit'])){

      $data_pelajaran = array(
        
        'nama_pelajaran'     => $_POST['nama_pelajaran'],
        'updated_at'         => date('Y-m-j ')
        
      );
      $data = $this->pelajaran->updateDataPelajaran($id,$data_pelajaran);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url('admin/pelajaran/listPelajaran'));
    
  }
  $this->template->back_views('site/back/pelajaranEdit',$data);
    
  }

  public function del_pelajaran($id){
    $this->pelajaran->delPelajaran($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/pelajaran/listPelajaran"));
}
}

/* End of file User.php */
