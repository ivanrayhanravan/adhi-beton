<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SlumpFlowTest extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SlumpFlowTest','SlumpFlowTest');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List  SlumpFlowTest
  public function listSlumpFlowTest(){
    $data['codepage']         = "back_addProduct";
    $data['page_title']   	  = ' List Slump Flow Test';
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();
    $data['sft']              = $this->SlumpFlowTest->getSlumpFlowTest()->result_array();
    $data['jmf']              = $this->SlumpFlowTest->getJobMixFormula()->result_array();
    $data['project']          = $this->SlumpFlowTest->getProject()->result_array();
      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SlumpFlowTest/listSlumpFlowTest');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/slumpFlowTestList',$data);
  }
    // List Slump Flow Test

    // Form Slump Flow Test
    public function formSlumpFlowTest($id){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	     = 'Form  Slump Flow Test';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['sft']                 = $this->SlumpFlowTest->getSlumpFlowTestById($id)->row_array();
      $data['project']             = $this->SlumpFlowTest->getProject()->result_array();
      $data['type']                = $this->SlumpFlowTest->getType()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SlumpFlowTest/formSlumpFlowTest');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
      $this->template->back_views('site/back/slumpFlowTestForm',$data);
    }
    // Form SlumpFlowTest

        // Edit dan View  SlumpFlowTest
    public function detailSlumpFlowTest($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Slump Flow Test";
      $data['sft']                   = $this->SlumpFlowTest->getSlumpFlowTestById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SlumpFlowTest/detailSlumpFlowTest/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/slumpFlowTestDetail',$data);

    }

    // End Edit dan View SlumpFlowTest

    public function delSlumpFlowTest($id){
      $data= $this->SlumpFlowTest->delSlumpFlowTest($id);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
      redirect(base_url("admin/SlumpFlowTest/listSlumpFlowTest"));
}

// Add Slump Flow Test
public function inputSlumpFlowTest(){
  $data['codepage']         = "back_addProduct";
  $data['page_title'] 	    = 'Add Slump Flow Test';
  $data['jmf']              = $this->SlumpFlowTest->getJobMixFormula()->result_array();

  {
   
    $idCache   = $_POST ['id_job_mix_formula'];
    $data_cache = array(
      'id_job_mix_formula'    => @$idCache,
     
    );
    $data = $this->SlumpFlowTest->inputCacheSlumpFlowTest($data_cache);

    $result              = $this->SlumpFlowTest->getCacheSlump()->row(); 
    $id_job_mix_formula  = $result->id_job_mix_formula;
    $id_product          = $result->id_product;
    $id_mutu             = $result->id_mutu;
    $status = 3;
    $kosong = 0;
    $data_sft  = array(
      'id_job_mix_formula'    => @$id_job_mix_formula,
      'id_product'            => @$id_product,
      'id_mutu'               => @$id_mutu,
      'id_project'            => @$kosong,
      'id_customer'           => @$kosong,
      'id_type'               => @$kosong,
      'id_status'             => @$status
    );
    $data = $this->SlumpFlowTest->inputSlumpFlowTest($data_sft);   
    $data = $this->SlumpFlowTest->deleteCacheSlump($data_cache);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SlumpFlowTest/listSlumpFlowTest"));    }

    // End Add Slump Flow Test

       // Update Slump Flow Test
       public function updateSlumpFlowTest($id=0){
        $data['codepage']           = "back_addProduct";
        $data['page_title'] 	      = 'Update Slump Flow';
        $data['SlumpFlowTest']      = $this->SlumpFlowTest->getSlumpFlowTestById($id)->row_array();
  
       {
          $config['upload_path']    ='./assets/img/content/SlumpFlowTest/';
          $config['allowed_types']  ='jpg|png|ico|pdf|docx';
    
          $this->load->library('upload',$config);
          if($this->upload->do_upload('doc_img'))
          {
              $img ='img/content/SlumpFlowTest/';
              $img.=  $this->upload->data('file_name');
          }
          $data_sft = array(
          
            'id_project'    => $_POST['id_project'],
            'id_customer'   => $_POST['id_customer'],
            'id_type'       => $_POST['id_type'],
            'admixture'     => $_POST['admixture'],
            'suhu'          => $_POST['suhu'],
            'id_type'       => $_POST['id_type'],
            'doc_img'       => @$img
          );
          $data = $this->SlumpFlowTest->updateSlumpFlowTest($id,$data_sft);
        }
        redirect(base_url("admin/SlumpFlowTest/listSlumpFlowTest"));
  
      }
          // End Slump Flow Test

  // Tolak Slump Test Flow
public function tolakSlumpFlowTest($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Tolak Slump Flow Test';
  $data['sft']                 = $this->SlumpFlowTest->getSlumpFlowTestById($id)->row_array();

  $status='2';
 {
    $data_slump = array(
    
        'id_status'                => @$status
    );

    $data = $this->SlumpFlowTest->tolakSlumpFlowTest($id,$data_slump);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
  redirect(base_url("admin/SlumpFlowTest/listSlumpFlowTest"));

}

// End Slump Test Flow

 // Setuju Slump Test Flow
 public function setujuSlumpFlowTest($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Tolak Slump Flow Test';
  $data['sft']                 = $this->SlumpFlowTest->getSlumpFlowTestById($id)->row_array();

  $status='1';
 {
    $data_slump = array(
    
        'id_status'                => @$status
    );

    $data = $this->SlumpFlowTest->setujuSlumpFlowTest($id,$data_slump);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil diterima !');  
  redirect(base_url("admin/SlumpFlowTest/listSlumpFlowTest"));

}

// End Slump Test Flow
    
    public function downloadArchive(){
      if (isset($_GET['filename'])) {
        $filename    = $_GET['filename'];
        $back_dir    ="assets/";
        $file = $back_dir.$_GET['filename'];
     
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: private');
            header('Pragma: private');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            
            exit;
        } 
        else {
            $_SESSION['pesan'] = "Oops! File - $filename - not found ...";
            header("location:archiveList.php");
        }
    }
  }

}

/* End of file User.php */
