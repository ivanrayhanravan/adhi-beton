<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class InternalTestKasar extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_InternalTestKasar','InternalTestKasar');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List Agregat InternalTestKasar
  public function listAgregatKasar(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= ' List Agregat Kasar';
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['internalKasar']  = $this->InternalTestKasar->getInternalKasar()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/InternalTestKasar/listAgregatKasar');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/agregatKasarList',$data);
  }
    // List Agregat InternalTestKasar

    // Form Agregat InternalTestKasar
    public function formAgregatKasar(){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	     = 'Form Add Agregat Kasar';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['sampleSource']        = $this->InternalTestKasar->getSampleSource()->result_array();
      $data['sampleDescription']   = $this->InternalTestKasar->getSampleDescription()->result_array();
      $data['pelaksana']           = $this->InternalTestKasar->getPelaksana()->result_array();
      $data['toBeUsed']            = $this->InternalTestKasar->getToBeUsed()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/InternalTestKasar/formAgregatKasar');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/agregatKasarForm',$data);
    }
    // Form Agregat InternalTestKasar

    // Add Agregat InternalTestKasar
    public function inputAgregatKasar(){
      $data['codepage']       = "back_addProduct";
      $data['page_title'] 	  = 'Add Agregat Kasar';
      $data['internalKasar']  = $this->InternalTestKasar->getInternalKasar()->result_array();
  
      {
        $config['upload_path']    ='./assets/img/content/InternalKasar/';
        $config['allowed_types']  ='jpg|png|ico';
        $config['encrypt_name']   = TRUE;
  
        $this->load->library('upload',$config);
        if($this->upload->do_upload('doc_input_satu'))
        {
            $img1 ='img/content/InternalKasar/';
            $img1.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_dua'))
        {
            $img2 ='img/content/InternalKasar/';
            $img2.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_tiga'))
        {
            $img3 ='img/content/InternalKasar/';
            $img3.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_empat'))
        {
            $img4 ='img/content/InternalKasar/';
            $img4.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_lima'))
        {
            $img5 ='img/content/InternalKasar/';
            $img5.=  $this->upload->data('file_name');
        }
       
        $status='3';
        $data_agregat_kasar = array(
        
          'id_sample_source'        => $_POST['id_sample_source'] ,
          'id_sample_description'   => $_POST['id_sample_description'],
          'id_pelaksana'            => $_POST['id_pelaksana'] ,
          'id_to_be_used'           => $_POST['id_to_be_used'] ,
          'received_date'           => date('Y-m-j'),
          'satu_input_satu'         => $_POST['satu_input_satu'] ,
          'satu_input_dua'          => $_POST['satu_input_dua'] ,
          'satu_input_tiga'         => $_POST['satu_input_tiga'] ,
          'satu_input_empat'        => $_POST['satu_input_empat'] ,
          'satu_input_lima'         => $_POST['satu_input_lima'] ,
          'satu_input_enam'         => $_POST['satu_input_enam'] ,
          'satu_input_tujuh'        => $_POST['satu_input_tujuh'] ,
          'satu_input_fm'           => $_POST['satu_input_fm'] ,
          'dua_input_satu'          => $_POST['dua_input_satu'] ,
          'dua_input_dua'           => $_POST['dua_input_dua'] ,
          'tiga_input_satu'         => $_POST['tiga_input_satu'] ,
          'empat_input_satu'        => $_POST['empat_input_satu'] ,
          'lima_input_satu'         => $_POST['lima_input_satu'],
          'doc_input_satu'          => @$img1,
          'doc_input_dua'           => @$img2,
          'doc_input_tiga'          => @$img3,
          'doc_input_empat'         => @$img4,
          'doc_input_lima'          => @$img5,
          'id_status'               => @$status
        );
        $data = $this->InternalTestKasar->inputAgregatKasar($data_agregat_kasar);
         
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url("admin/InternalTestKasar/listAgregatKasar"));    }

        // End Add Agregat InternalTestKasar

        // Edit dan View  InternalTestKasar
    public function detailInternalKasar($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Internal Kasar";
      $data['internalKasar']         = $this->InternalTestKasar->getInternalKasarById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/InternalTestKasar/detailInternalKasar/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/agregatKasarDetail',$data);

    }

    // End Edit dan View InternalTestKasar


  // Tolak Incoming InternalTestKasar
public function tolakInternalKasar($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Tolak Incoming Kasar';
  $data['internalKasar']       = $this->InternalTestKasar->getInternalKasarById($id)->row_array();

  $status='2';
 {
    $data_internal_kasar = array(
    
        'id_status'                => @$status
    );

    $data = $this->InternalTestKasar->tolakInternalKasar($id,$data_internal_kasar);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
  redirect(base_url("admin/InternalTestKasar/listAgregatKasar"));

}
// End Tolak InternalTestKasar

// Setuju InternalTestKasar
public function setujuInternalKasar($id=0){
  $data['codepage']              = "back_addProduct";
  $data['page_title'] 	         = 'Setuju Internal Kasar';
  $data['internalKasar']         = $this->InternalTestKasar->getInternalKasarById($id)->row_array();

  $status='1';
 {
    $data_internal_kasar = array(
    
        'id_status'                => @$status
    );

    $data = $this->InternalTestKasar->setujuInternalKasar($id,$data_internal_kasar);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
  redirect(base_url("admin/InternalTestKasar/listAgregatKasar"));

}
// End Setuju InternalTestKasar

public function delInternalKasar($id){
  $data= $this->InternalTestKasar->delInternalKasar($id);
   $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
   redirect(base_url("admin/InternalTestKasar/listAgregatKasar"));
}

  

 

}

/* End of file User.php */
