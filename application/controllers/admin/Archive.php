<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Archive extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_Archive','Archive');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List Agregat Archive
  public function listArchive(){
    $data['codepage']         = "back_addProduct";
    $data['page_title']   	  = ' List Archive Test';
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();
    $data['archive']          = $this->Archive->getArchive()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Archive/listArchive');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/archiveList',$data);
  }
    // List Agregat Archive

    // Form Agregat Archive
    public function formArchive(){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	   = 'Form Add Archive';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['sampleSource']        = $this->Archive->getSampleSource()->result_array();
      $data['pelaksana']           = $this->Archive->getPelaksana()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Archive/formArchive');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/archiveForm',$data);
    }
    // Form Agregat Archive

    // Add Agregat Archive
    public function inputArchive(){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Add Archive';
      $data['archive']          = $this->Archive->getArchive()->result_array();
  
      {
        $config['upload_path']    ='./assets/img/content/Archive/';
        $config['allowed_types']  ='jpg|png|ico|pdf|docx';
  
        $this->load->library('upload',$config);
        if($this->upload->do_upload('sertif_img'))
        {
            $img ='img/content/Archive/';
            $img.=  $this->upload->data('file_name');
        }
       
        $data_archive = array(
          'jenis_test'              => $_POST['jenis_test'] ,
          'nama_test'               => $_POST['nama_test'] ,
          'id_sample_source'        => $_POST['id_sample_source'] ,
          'id_pelaksana'            => $_POST['id_pelaksana'] ,
          'received_date'           => date('Y-m-j'),
          'sertif_img'              => @$img,
        );
        $data = $this->Archive->inputArchive($data_archive);
         
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url("admin/Archive/listArchive"));    }

        // End Add Archive

        // Edit dan View  Archive
    public function detailArchive($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Archive";
      $data['archive']               = $this->Archive->getArchiveById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Archive/detailArchive/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/archiveDetail',$data);

    }

    // End Edit dan View Archive

    public function delArchive($id){
      $data= $this->Archive->delArchive($id);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
      redirect(base_url("admin/Archive/listArchive"));
}
    
    public function downloadArchive(){
      if (isset($_GET['filename'])) {
        $filename    = $_GET['filename'];
        $back_dir    ="assets/";
        $file = $back_dir.$_GET['filename'];
     
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: private');
            header('Pragma: private');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            
            exit;
        } 
        else {
            $_SESSION['pesan'] = "Oops! File - $filename - not found ...";
            header("location:archiveList.php");
        }
    }
  }

}

/* End of file User.php */
