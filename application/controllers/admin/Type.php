<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Type extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_type','type');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Type
   public function listType(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Type';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['type']           = $this->type->getType()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Type/listType');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/typeList',$data);
  }
    // End List Type

 // Add Type
 public function inputType(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Type';
 {
    $data_type = array(
    
      'name_type'   => $_POST['name_type']
    );
    $data = $this->type->inputType($data_type);
  }
  redirect(base_url("admin/Type/listType"));

}
// End Add Pelaksana

    // Form Type
    public function formType(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Type';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Type/formType');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/typeForm',$data);
    }
    // End Form Type

      // Form Edit Type
      public function formEditType($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Type';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['type']           = $this->type->getTypeById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Type/formEditType');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/typeEditForm',$data);
      }
      // End Form Edit Type

       // Update type
    public function updateType($id=0){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Update Type';
      $data['type']             = $this->type->getTypeById($id)->row_array();

     {
        $data_type = array(
        
          'name_type'       => $_POST['name_type']
        );
        $data = $this->type->updateType($id,$data_type);
      }
      redirect(base_url("admin/Type/listType"));

    }

        // End Update type

  
//   Delete Data type
  public function delType($id){
    $data= $this->type->delType($id);
    $this->type->delType($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Type/listType"));
  }
//   Delete Data type

}

/* End of file User.php */
