<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_Vendor','vendor');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Vendor
   public function listVendor(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Vendor';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['vendor']              = $this->vendor->getVendor()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Vendor/listVendorr');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/vendorList',$data);
  }
    // End List  Vendor

 // Add Vendor
 public function inputVendor(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Vendor';
 {
    $data_vendor = array(
    
      'vendor_name'   => $_POST['vendor_name']
    );
    $data = $this->vendor->inputVendor($data_vendor);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/Vendor/listVendor"));

}
// End Add Vendor

    // Form Vendor
    public function formVendor(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Vendor';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Vendor/formVendor');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/vendorForm',$data);
    }
    // End Form Vendor

      // Form Edit Vendor
      public function formEditVendor($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Vendor';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['vendor']              = $this->vendor->getVendorById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Vendor/formEditVendor');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/vendorEditForm',$data);
      }
      // End Form Edit Vendor

       // Update Vendor
    public function updateVendor($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Vendor';
      $data['vendor']                = $this->vendor->getVendorById($id)->row_array();

     {
        $data_vendor = array(
        
          'vendor_name'              => $_POST['vendor_name']
        );
        $data = $this->vendor->updateVendor($id,$data_vendor);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/Vendor/listVendor"));

    }

        // End Update Vendor

  
//   Delete Data Vendor
  public function delVendor($id){
    $data= $this->vendor->delVendor($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Vendor/listVendor"));
  }
//   Delete Data Vendor
}

/* End of file User.php */
