<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaksana extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_pelaksana','pelaksana');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Project
   public function listPelaksana(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Pelaksana';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['pelaksana']      = $this->pelaksana->getPelaksana()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Pelaksana/listPelaksana');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/pelaksanaList',$data);
  }
    // End List Customer

 // Add Pelaksana
 public function inputPelaksana(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Pelaksana';
 {
    $data_pelaksana = array(
    
      'name_pelaksana'   => $_POST['name_pelaksana']
    );
    $data = $this->pelaksana->inputPelaksana($data_pelaksana);
  }
  redirect(base_url("admin/Pelaksana/listPelaksana"));

}
// End Add Pelaksana

    // Form Pelaksana
    public function formPelaksana(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Pelaksana';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Pelaksana/formPelaksana');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/pelaksanaForm',$data);
    }
    // End Form Pelaksana

      // Form Edit Pelaksana
      public function formEditPelaksana($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Pelaksana';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['pelaksana']      = $this->pelaksana->getPelaksanaById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Pelaksana/formEditPelaksana');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/pelaksanaEditForm',$data);
      }
      // End Form Edit Pelaksana

       // Update Pelaksana
    public function updatePelaksana($id=0){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Update Pelaksana';
      $data['pelaksana']        = $this->pelaksana->getPelaksanaById($id)->row_array();

     {
        $data_pelaksana = array(
        
          'name_pelaksana'       => $_POST['name_pelaksana']
        );
        $data = $this->pelaksana->updatePelaksana($id,$data_pelaksana);
      }
      redirect(base_url("admin/Pelaksana/listPelaksana"));

    }

        // End Update Pelaksana

  
//   Delete Data Pelaksana
  public function delPelaksana($id){
    $data= $this->pelaksana->delPelaksana($id);
    $this->pelaksana->delPelaksana($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Pelaksana/listPelaksana"));
  }
//   Delete Data Pelaksana

}

/* End of file User.php */
