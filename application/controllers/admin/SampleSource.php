<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SampleSource extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SampleSource','SampleSource');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Sample Source
   public function listSampleSource(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Sample Source';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['sampleSource']   = $this->SampleSource->getSampleSource()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SampleSource/listSampleSource');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sampleSourceList',$data);
  }
    // End List Sample Source

 // Add Sample Source
 public function inputSampleSource(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sample Source';
 {
    $data_sample_source = array(
    
      'sample_source_name'   => $_POST['sample_source_name']
    );
    $data = $this->SampleSource->inputSampleSource($data_sample_source);
  }
  redirect(base_url("admin/SampleSource/listSampleSource"));

}
// End Add Sample Source

    // Form Sample Source
    public function formSampleSource(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sample Source';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SampleSource/formSampleSource');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sampleSourceForm',$data);
    }
    // End Form Sample Source   

      // Form Edit Sample Source
      public function formEditSampleSource($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Sample Source';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['sampleSource']   = $this->SampleSource->getSampleSourceById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SampleSource/formEditSampleSource');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sampleSourceEditForm',$data);
      }
      // End Form Edit Sample Source

       // Update Sample Source
    public function updateSampleSource($id=0){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Update Sample Source';
      $data['sampleSource']     = $this->SampleSource->getSampleSourceById($id)->row_array();

     {
        $data_sample_source = array(
        
          'sample_source_name'       => $_POST['sample_source_name']
        );
        $data = $this->SampleSource->updateSampleSource($id,$data_sample_source);
      }
      redirect(base_url("admin/SampleSource/listSampleSource"));

    }

        // End Update Sample Source

  
//   Delete Data Sample Source
  public function delSampleSource($id){
    $data= $this->SampleSource->delSampleSource($id);
    $this->SampleSource->delSampleSource($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SampleSource/listSampleSource"));
  }
//   Delete Data Sample Source

}

/* End of file User.php */
