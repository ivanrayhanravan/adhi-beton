<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_Customer','customer');
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_status','status');
    $this->load->library('email');
    $this->load->library('session');
    
  }


  // List Customer
  public function listCustomer(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'Customer';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['customer']       = $this->customer->getCustomer()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/customer/listCustomer');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }
    $this->template->back_views('site/back/customerList',$data);
  }
    // End List Customer

    // Form Add Customer
    public function formCustomer(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']   	= 'Form Add Customer';
      $data['status']         = $this->status->getListStatus()->result_array();
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Customer/formCustomer');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/customerForm',$data);
    }
    // End Form Add Customer

       // Form Edit Customer
       public function formEditCustomer($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Customer';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['customer']       = $this->customer->getCustomerById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Customer/formEditCustomer');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/customerEditForm',$data);
      }
      // End Form Edit Customer

        // Update Customer
    public function updateCustomer($id=0){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Update Customer';
      $data['customer']     = $this->customer->getCustomerById($id)->row_array();

     {
        $data_customer = array(
        
          'customer_name'      => $_POST['name'] 
        );
        $data = $this->customer->updateCustomer($id,$data_customer);
      }
      redirect(base_url("admin/Customer/listCustomer"));

    }

        // End Update Customer

    // Add Customer
    public function inputCustomer(){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Add Agregat Halus';
      $data['status']       = $this->status->getListStatus()->result_array();
      $data['customer']     = $this->customer->getCustomer()->result_array();
     {
        $data_customer = array(
        
          'customer_name'      => $_POST['name'] 
        );
        $data = $this->customer->inputCustomer($data_customer);
        $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      }
      redirect(base_url("admin/Customer/listCustomer"));

    }

        // End Add Customer
  
  public function delCustomer($id){
    $data= $this->customer->delCustomer($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Customer/listCustomer"));
}
}

/* End of file User.php */
