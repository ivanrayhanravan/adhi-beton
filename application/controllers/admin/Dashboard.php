<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends PIS_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_dashboard','dashboard');

    
  }

  public function index()
  {
    $data['codepage'] = "back_index";
    $data['page_title'] 	       = 'Dashboard';
    $data['subpage_title'] 	       = 'Penjelasan Halaman';
    $id = $_SESSION['id'];
    $data['image']    = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Dashboard/index');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    // $data['hitung'] = $this->dashboard->hitung_santri($santri)->result_array();
   
    $this->template->back_views('site/back/dashboard',$data);
  }

}

/* End of file Dashboard.php */
