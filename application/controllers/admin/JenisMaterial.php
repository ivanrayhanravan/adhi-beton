<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisMaterial extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_JenisMaterial','JenisMaterial');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Project
   public function listJenisMaterial(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Jenis Material';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['jenisMaterial']  = $this->JenisMaterial->getJenisMaterial()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/JenisMaterial/listJenisMaterial');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/jenisMaterialList',$data);
  }
    // End List Customer

 // Add Jenis Material
 public function inputJenisMaterial(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Jenis Material';
 {
    $data_material = array(
    
      'name_material'   => $_POST['name_material']
    );
    $data = $this->JenisMaterial->inputJenisMaterial($data_material);
  }
  redirect(base_url("admin/JenisMaterial/listJenisMaterial"));

}
// End Add Jenis material

    // Form Jenis Material
    public function formJenisMaterial(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']   	= 'Form Add Jenis Material';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/JenisMaterial/formJenisMaterial');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/jenisMaterialForm',$data);
    }
    // End Form Jenis Material

      // Form Edit Jenis Material
      public function formEditJenisMaterial($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Jenis Material';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['jenisMaterial']  = $this->JenisMaterial->getJenisMaterialById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/JenisMaterial/formEditJenisMaterial');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/jenisMaterialEditForm',$data);
      }
      // End Form Edit Jenis Material

       // Update Jenis Material
    public function updateJenisMaterial($id=0){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Update Jenis Material';
      $data['jenisMaterial']    = $this->JenisMaterial->getJenisMaterialById($id)->row_array();

     {
        $data_material = array(
        
          'name_material'       => $_POST['name_material']
        );
        $data = $this->JenisMaterial->updateJenisMaterial($id,$data_material);
      }
      redirect(base_url("admin/JenisMaterial/listJenisMaterial"));

    }

        // End Update Jenis Material

  
//   Delete Data Jenis Material
  public function delJenisMaterial($id){
    $data= $this->JenisMaterial->delJenisMaterial($id);
    $this->JenisMaterial->delJenisMaterial($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/JenisMaterial/listJenisMaterial"));
  }
//   Delete Data Jenis Material

}

/* End of file User.php */
