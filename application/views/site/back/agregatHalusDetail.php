<div class="container-fluid" data-codepage="<?php echo $codepage ?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-5 m-t-5">
                        <img src="<?= img_url($internalHalus['doc_input_satu'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 1</h4>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-10 m-t-10">
                        <img src="<?= img_url($internalHalus['doc_input_dua'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 2</h4>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-10 m-t-10">
                        <img src="<?= img_url($internalHalus['doc_input_tiga'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 3</h4>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-10 m-t-10">
                        <img src="<?= img_url($internalHalus['doc_input_empat'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 4</h4>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-10 m-t-10">
                        <img src="<?= img_url($internalHalus['doc_input_lima'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 5</h4>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-10 m-t-10">
                        <img src="<?= img_url($internalHalus['doc_input_enam'])?>" width="120" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi 6</h4>
                    </div>
                </div>
            </div>

        </div>
        
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">View</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Sample Source</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $internalHalus['sample_source_name']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Sample Description</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" id="fullname" name="fullname" value="<?= $internalHalus['sample_description_name']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Received Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $internalHalus['received_date']?>">
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Tested By</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['name_pelaksana']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">To Be Used</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['name_to_be_used']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_dua']?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_tiga']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_empat']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_lima']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_enam']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_tujuh']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Fine Moduls of Sans (FM) :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['satu_input_fm']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['dua_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['dua_input_dua']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['tiga_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['empat_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['lima_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Spec Req :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['enam_input_satu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $internalHalus['status']?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
