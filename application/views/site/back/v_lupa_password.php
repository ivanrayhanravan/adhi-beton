<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<div class="container" data-codepage="<?= $codepage?>">
  <div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
    <div class="col-sm-12 col-md-4">

          <form  action="<?= base_url('admin/user/lupa_password')?>" method="POST">
              <div class="form-group ">
              
                <label  for="forgotemail">Masukkan Email untuk Reset Password</label>
                <input id="userEmailAdmin" type="email" class="form-control" name="email" placeholder="Email">
                <br>
                <div class="alert alert-danger" id="format-pwd-invalid"  style="display:none;" for="forgot">Format email salah, cek kembali penulisan email</div>
                <div class="alert alert-danger" id="forgot-pwd-invalid"  style="display:none;" class="errorreg " for="forgot">Email Tidak Tersedia</div>
                <div class="alert alert-info" id="forgot-valid"  style="display:none;" class="success" for="forgot">Email Tersedia</div>
              </div>
              <button type="submit" name="submit" class="btn btn-danger w-100">Kirim</button>
          </form>


      </div>
  </div>  
<div>

 