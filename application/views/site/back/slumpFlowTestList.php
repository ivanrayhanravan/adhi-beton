<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
<?php $isEdit = $page_title == "Perbarui Produk"? true: false; ?>

	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>
					
			<form id="regForm" enctype="multipart/form-data"  method="post" action="<?php echo base_url('admin/SlumpFlowTest/inputSlumpFlowTest/')?>"  data-dir="" data-url="">
				<div class="form-group col-sm-4 ">
					<b>Pilih Job Mix Formula yang akan digunakan ! </b>
			  			<select class="form-control" name="id_job_mix_formula">
						  <?php foreach($jmf as $r):?>
                            <option value="<?php echo  $r['id_job_mix_formula']?>"><?php echo $r['name_jmf'] ?></option>
						  <?php endforeach ?>
						</select>
					<div class="form-group text-left">
						<button class="btn btn-facebook btn-sm waves-effect waves-light" type="submit" name="submit"><span
								class="btn-label"><i class="fas fa-plus"> </i></span> Add JMF For Slump</button>
					</div>				
				</div>



					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
                                    <th>Nama JMF</th>
									<th>Product</th>
									<th>Customer</th>
                                    <th>Project</th>
                                    <th>Suhu</th>
                                    <th>Date Time</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <?php foreach($sft as $r):?>
                <tr>
                    <td><?= $no++?></td>
					<?php if($r['doc_img']== NULL ){?>
                    <td><?php echo $r['name_jmf'] ?>&nbsp<a href="<?php echo base_url('admin/SlumpFlowTest/formSlumpFlowTest/'.$r['id_slump_flow_data'])?>"> <button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/formSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-plus"> Add Slump</i></button></td>
					<?php } ?> 
					<?php if($r['doc_img']== !NULL ){?>
					<td><?php echo $r['name_jmf'] ?>&nbsp<a href="<?php echo base_url('admin/SlumpFlowTest/formEditSlumpFlowTest/'.$r['id_slump_flow_data'])?>"> </td>
					<?php } ?> 
					<td><?php echo $r['name_product'] ?></td>
					<td><?php echo $r['customer_name'] ?></td>
                    <td><?php echo $r['project_name'] ?></td>
                    <td><?php echo $r['suhu'] ?></td>
                    <td><?php echo $r['date_time'] ?></td>
					<td><?php echo $r['status'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/SlumpFlowTest/delSlumpFlowTest/'.$r['id_slump_flow_data'])?>" onclick="return confirm('Apakah anda yakin ingin menghapus data ?')"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/delSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-trash"></i></button>
                    <?php if($r['doc_img']== !NULL ){?>
					<a href="<?php echo base_url('admin/SlumpFlowTest/detailSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/detailSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-pencil-alt"></i></button><br>
					<a href="<?php echo base_url('admin/SlumpFlowTest/detailSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/detailSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-print"> Print</i></button><br>
					<?php } ?> 

					<?php if($r['id_status']== 1 ){?>
					<a href="<?php echo base_url('admin/SlumpFlowTest/tolakSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/tolakSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-false">TOLAK</i></button><br>
					<?php } ?> 

					<?php if($r['id_status']== 2 ){?>
					<a href="<?php echo base_url('admin/SlumpFlowTest/setujuSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/setujuSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-pencil">SETUJU</i></button>
					<?php } ?> 

					<?php if($r['id_status']== 3 ){?>
					<a href="<?php echo base_url('admin/SlumpFlowTest/tolakSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/tolakSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-false">TOLAK</i></button><br>
					<a href="<?php echo base_url('admin/SlumpFlowTest/setujuSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SlumpFlowTest/setujuSlumpFlowTest/'.$r['id_slump_flow_data'])?>"><i class="fas fa-pencil">SETUJU</i></button>
					<?php } ?> 
			    </tr>
                <?php endforeach ?>
      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
	</form>
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->