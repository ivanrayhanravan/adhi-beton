<!-- Login box.scss -->
<!-- ============================================================== -->
<div class="auth-wrapper bg-dark d-flex no-block justify-content-center align-items-center"
	style="background:url(<?php echo vendor_url('back/images/big/auth-bg.jpg'); ?>) no-repeat center center;">
	<div class="auth-box">
		<div id="loginform">
			<div class="logo">
				<span class="db"><img src="<?php echo vendor_url('back/images/logo-icon.png'); ?>" alt="logo" /></span>
				<h5 class="font-medium m-b-20">Sign In to Admin</h5>
			</div>
			<?php if(!empty($_SESSION['fail_msg_password'])):?>
				<div class="alert alert-danger" role="alert">
					<?php echo $_SESSION['fail_msg_password']?>
				</div>
			<?php elseif(!empty($_SESSION['fail_msg_account'])):?>
			<div class="alert alert-danger" role="alert">
				<?php echo $_SESSION['fail_msg_account']?>
			</div>
			<?php endif;?>
			<?php if(!empty($_SESSION['fail_msg_register'])):?>
				<div class="alert alert-danger" role="alert">
					<?php echo $_SESSION['fail_msg_register']?>
				</div>
			<?php elseif(!empty($_SESSION['fail_msg_account'])):?>
			<div class="alert alert-danger" role="alert">
				<?php echo $_SESSION['fail_msg_account']?>
			</div>
			<?php endif;?>


			<?php if(!empty($_SESSION['sukses'])):?>
         		<div class="alert alert-success" role="alert">
            <?php echo $_SESSION['sukses']?>
          		</div>
        	<?php elseif(!empty($_SESSION['gagal'])):?>
        		<div class="alert alert-danger" role="alert">
          	<?php echo $_SESSION['gagal']?>
        		</div>
        	<?php endif;?>
			<!-- Form -->
			<div class="row">
				<div class="col-12">
					<form class="form-horizontal m-t-20" id="loginform"
						action="<?php echo base_url('login_admin'); ?>" method="POST">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
							</div>
							<input type="text" name="email" class="form-control form-control-lg" placeholder="Username" aria-label="Username"
								aria-describedby="basic-addon1">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
							</div>
							<input type="password" name="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password"
								aria-describedby="basic-addon1">
						</div>
						
						<div class="form-group text-center">
							<div class="col-xs-12 p-b-20">
								<button class="btn btn-block btn-lg btn-info" type="submit">Log In</button>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1">
									<a href="<?= base_url('admin/user/lupa_password')?>" id="to-recover" class="text-dark float-right"><i
											class="fa fa-lock m-r-5"></i> Lupa Password?</a>
								</div>
							</div>
						</div>
						
					
					</form>
				</div>
			</div>
		</div>
		<div id="recoverform">
			<div class="logo">
				<span class="db"><img src="<?php echo vendor_url('back/images/logo-icon.png'); ?>" alt="logo" /></span>
				<h5 class="font-medium m-b-20">Please wait for Recover Password</h5>
			</div>
			
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- Login box.scss -->